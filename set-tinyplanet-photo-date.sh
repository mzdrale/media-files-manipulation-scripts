#!/bin/bash
#
# Usage example:
#    for file in *_*_*.*; do $0 $file; done
#

FILE=$1

DATE=$(echo $FILE | awk -F\. '{print $1}' | awk -F\_ '{print $3}')
TIME=$(echo $FILE | awk -F\. '{print $1}' | awk -F\_ '{print $4}')
TIME_HM=${TIME:0:4}
TIME_S=${TIME:4:2}

echo "$FILE --> $DATE$TIME_HM.$TIME_S"
touch -t $DATE$TIME_HM.$TIME_S $FILE

