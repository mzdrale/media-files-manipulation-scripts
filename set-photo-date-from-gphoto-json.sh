#!/bin/bash

#❯ touch -t $(date -r $(cat image-0-02-04-0788704a20cbcc7109355f3833fe0434.json | jq -r .creationTime.timestamp) +"%Y%m%d%H%m.%S") image-0-02-05-fdcc6f31deb47946a4736acf588327252.jpg

FILE="$1"

FILE_NAME="${FILE%.*}"
FILE_EXTENSION="${FILE##*.}"
# echo "${FILE_NAME} - ${FILE_EXTENSION}"

#JSON_FILE="${FILE_NAME%?}.json"
JSON_FILE="${FILE}.json"

# echo "${FILE}: ${JSON_FILE}"

CREATE_TIMESTAMP=$(cat ${JSON_FILE} | jq -r .creationTime.timestamp)
CREATE_TIME=$(date -r ${CREATE_TIMESTAMP} +"%Y%m%d%H%m.%S")

echo "${FILE} --> ${CREATE_TIME}"
touch -t ${CREATE_TIME} ${FILE}
