#!/bin/bash

FILE=$1

if [[ "x${FILE}" == "x" ]]; then
	echo
	echo "Usage: $0 <IMAGE_FILE_NAME>"
	echo
fi

EXIF_DATETIME=$(exif -d "${FILE}" 2>&1 |egrep "Date and Time \(Ori.+\|" |awk -F\| '{print $2}')
DATE=$(echo ${EXIF_DATETIME} |awk '{print $1}' |sed -e 's/\://g')
TIME=$(echo ${EXIF_DATETIME} |awk '{print $2}' |sed -e 's/\://g')
TIME_HM=${TIME:0:4}

# EXIF_DATETIME=$(mdls $FILE 2>&1 | egrep "^kMDItemContentCreationDate\s+\=" | awk -F"=" '{print $2}')
# echo $FILE $EXIF_DATETIME
# DATE=$(echo $EXIF_DATETIME |awk '{print $1}' |sed -e 's/[\:-]//g')
# TIME=$(echo $EXIF_DATETIME |awk '{print $2}' |sed -e 's/\://g')
# TIME_HM=${TIME:0:4}

# Offset +2h (0100 = 100)
#TIME_HM=$((${TIME:0:4}+100))
TIME_S=${TIME:4:2}

echo "${FILE} --> ${DATE}${TIME_HM}.${TIME_S}"
touch -t ${DATE}${TIME_HM}.${TIME_S} "${FILE}"



