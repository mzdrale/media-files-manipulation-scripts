#!/bin/bash

INPUT_FILE=$1
START_AT=$2
DURATION=$3

if [[ "${INPUT_FILE}" == "" ]]; then
	echo "Usage: $0 \"<input_file>\" <start_at> <duration_in_seconds>"
	exit 1
fi

if [[ "${START_AT}" == "" ]]; then
        echo "Usage: $0 \"<input_file>\" <start_at> <duration_in_seconds>"
        exit 1
fi

if [[ "${DURATION}" == "" ]]; then
        echo "Usage: $0 \"<input_file>\" <start_at> <duration_in_seconds>"
        exit 1
fi


CNT=1
OUTPUT_FILE="trimed-$CNT-$INPUT_FILE"
while [ -e $OUTPUT_FILE ]; do
	CNT=$(($CNT+1))
	OUTPUT_FILE="trimed-$CNT-$INPUT_FILE"
done

echo "ffmpeg -ss $START_AT -i \"$INPUT_FILE\" -t $DURATION -vcodec copy -acodec copy \"$OUTPUT_FILE\""
echo "No sound '-an' instaed '-acodec copy'"

